require(`dotenv`).config({
  path: `.env.${process.env.NODE_ENV}`
});

module.exports = {
  siteMetadata: {
    title: `Color Tool`,
    description: `A simple tool for working with website color`,
    author: `@bobhumphrey`,
    siteUrl: `https://color.bob-humphrey.com`
  },
  plugins: [
    `gatsby-plugin-sharp`,
    `gatsby-transformer-sharp`,
    `gatsby-plugin-react-helmet`,
    `gatsby-plugin-postcss`,
    {
      resolve: `gatsby-plugin-purgecss`,
      options: {
        tailwind: true,
        purgeOnly: [`src/css/style.css`],
        whitelistPatterns: [/^bg-/, /^text-/]
      }
    },
    `gatsby-plugin-offline`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        path: `${__dirname}/src/images`,
        name: `images`
      }
    },
    {
      resolve: `gatsby-plugin-robots-txt`,
      options: {
        host: `https://color.bob-humphrey.com`,
        env: {
          development: {
            policy: [{ userAgent: `*`, disallow: [`/`] }]
          },
          production: {
            policy: [{ userAgent: `*`, disallow: [`/`] }]
          }
        }
      }
    }
  ]
};
