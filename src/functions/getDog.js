export default function getDog(key) {
  const dogs = {
    "1.jpg": {
      credit: "Jordan Nelson",
      url: "jwebbnelson"
    },
    "2.jpg": {
      credit: "Marek Szturc",
      url: "m_maaris"
    },
    "4.jpg": {
      credit: "Marcus Löfvenberg",
      url: "marcuslofvenberg"
    },
    "8.jpg": {
      credit: "César Cardoso",
      url: "unsubtl"
    },
    "10.jpg": {
      credit: "Alexandru Rotariu",
      url: "rotalex"
    },
    "12.jpg": {
      credit: "Radka Kocanova",
      url: "radka_k"
    },
    "17.jpg": {
      credit: "Ja San Miguel",
      url: "roastedtuna"
    },
    "19.jpg": {
      credit: "Itay Kabalo",
      url: "itaykabalo"
    },
    "25.jpg": {
      credit: "Nathalie SPEHNER",
      url: "nathalie_spehner"
    }
  };
  return dogs[key];
}
