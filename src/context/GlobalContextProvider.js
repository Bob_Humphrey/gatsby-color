import React from "react";

export const GlobalStateContext = React.createContext();
export const GlobalDispatchContext = React.createContext();

const initialState = {
  colorSet: "tailwind",
  backgroundColor: "blue-100",
  primaryColor: "red-700",
  secondaryColor: "blue-400",
  primaryTextColor: "text-gray-900",
  secondaryTextColor: "text-blue-700",
  tailwindBackgroundColor: "blue-100",
  tailwindPrimaryColor: "red-700",
  tailwindSecondaryColor: "blue-400",
  tailwindPrimaryTextColor: "text-gray-900",
  tailwindSecondaryTextColor: "text-blue-700",
  materialBackgroundColor: "md-blue-50",
  materialPrimaryColor: "md-red-700",
  materialSecondaryColor: "md-blue-400",
  materialPrimaryTextColor: "text-md-gray-900",
  materialSecondaryTextColor: "text-md-blue-700",
  pickerLabel: "",
  pickerProperty: "",
  showColorPicker: false,
  showSettings: false
};

function reducer(state, action) {
  switch (action.type) {
    case `PICK_COLOR`: {
      return {
        ...state,
        pickerLabel: action.label,
        pickerProperty: action.property,
        showColorPicker: true,
        showSettings: false
      };
    }
    case `PICK_COLOR_CANCEL`: {
      return {
        ...state,
        showColorPicker: false
      };
    }
    case `CHANGE_COLOR`: {
      const hue = action.hue;
      const value = action.value;
      const property = action.property;
      const newValue = determineValue(property, hue, value);
      return {
        ...state,
        [property]: newValue,
        showColorPicker: false
      };
    }
    case `SETTINGS`: {
      return {
        ...state,
        showSettings: true,
        showColorPicker: false
      };
    }
    case `SETTINGS_CANCEL`: {
      return {
        ...state,
        showSettings: false
      };
    }
    case `SETTINGS_CHANGE`: {
      if (action.colorSet === "tailwind") {
        const newMaterialBackgroundColor = state.backgroundColor;
        const newMaterialPrimaryColor = state.primaryColor;
        const newMaterialSecondaryColor = state.secondaryColor;
        const newMaterialPrimaryTextColor = state.primaryTextColor;
        const newMaterialSecondaryTextColor = state.secondaryTextColor;
        const newBackgroundColor = state.tailwindBackgroundColor;
        const newPrimaryColor = state.tailwindPrimaryColor;
        const newSecondaryColor = state.tailwindSecondaryColor;
        const newPrimaryTextColor = state.tailwindPrimaryTextColor;
        const newSecondaryTextColor = state.tailwindSecondaryTextColor;
        return {
          ...state,
          materialBackgroundColor: newMaterialBackgroundColor,
          materialPrimaryColor: newMaterialPrimaryColor,
          materialSecondaryColor: newMaterialSecondaryColor,
          materialPrimaryTextColor: newMaterialPrimaryTextColor,
          materialSecondaryTextColor: newMaterialSecondaryTextColor,
          backgroundColor: newBackgroundColor,
          primaryColor: newPrimaryColor,
          secondaryColor: newSecondaryColor,
          primaryTextColor: newPrimaryTextColor,
          secondaryTextColor: newSecondaryTextColor,
          colorSet: "tailwind"
        };
      } else if (action.colorSet === "material") {
        const newTailwindBackgroundColor = state.backgroundColor;
        const newTailwindPrimaryColor = state.primaryColor;
        const newTailwindSecondaryColor = state.secondaryColor;
        const newTailwindPrimaryTextColor = state.primaryTextColor;
        const newTailwindSecondaryTextColor = state.secondaryTextColor;
        const newBackgroundColor = state.materialBackgroundColor;
        const newPrimaryColor = state.materialPrimaryColor;
        const newSecondaryColor = state.materialSecondaryColor;
        const newPrimaryTextColor = state.materialPrimaryTextColor;
        const newSecondaryTextColor = state.materialSecondaryTextColor;
        return {
          ...state,
          tailwindBackgroundColor: newTailwindBackgroundColor,
          tailwindPrimaryColor: newTailwindPrimaryColor,
          tailwindSecondaryColor: newTailwindSecondaryColor,
          tailwindPrimaryTextColor: newTailwindPrimaryTextColor,
          tailwindSecondaryTextColor: newTailwindSecondaryTextColor,
          backgroundColor: newBackgroundColor,
          primaryColor: newPrimaryColor,
          secondaryColor: newSecondaryColor,
          primaryTextColor: newPrimaryTextColor,
          secondaryTextColor: newSecondaryTextColor,
          colorSet: "material"
        };
      }
      break;
    }
    default:
      throw new Error(`Bad Action Type`);
  }
}

function determineValue(property, hue, value) {
  switch (property) {
    case "backgroundColor":
      if (hue === "black") return "black";
      if (hue === "white") return "white";
      return hue + "-" + value;

    case "primaryColor":
      if (hue === "black") return "black";
      if (hue === "white") return "white";
      return hue + "-" + value;

    case "secondaryColor":
      if (hue === "black") return "black";
      if (hue === "white") return "white";
      return hue + "-" + value;

    case "primaryTextColor":
      if (hue === "black") return "text-black";
      if (hue === "white") return "text-white";
      return "text-" + hue + "-" + value;

    case "secondaryTextColor":
      if (hue === "black") return "text-black";
      if (hue === "white") return "text-white";
      return "text-" + hue + "-" + value;

    default:
      break;
  }
}

const GlobalContextProvider = ({ children }) => {
  const [state, dispatch] = React.useReducer(reducer, initialState);
  return (
    <GlobalStateContext.Provider value={state}>
      <GlobalDispatchContext.Provider value={dispatch}>
        {children}
      </GlobalDispatchContext.Provider>
    </GlobalStateContext.Provider>
  );
};

export default GlobalContextProvider;
