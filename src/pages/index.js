import React, { useContext } from "react";
import AppLayout from "../components/AppLayout";
import { GlobalStateContext } from "../context/GlobalContextProvider";
import Material from "../components/Material";
import SettingsHome from "../components/SettingsHome";

const IndexPage = props => {
  const state = useContext(GlobalStateContext);
  return (
    <AppLayout pathname={props.path}>
      <div className={"bg-white pt-24 pb-12"}>
        <div className="w-3/4 xl:w-1/2 mx-auto">
          <div
            className={
              state.primaryTextColor +
              " text-5xl font-chunk text-center leading-tight"
            }
          >
            A simple tool
          </div>
          <div
            className={
              state.secondaryTextColor +
              " text-5xl font-chunk text-center leading-tight"
            }
          >
            for working with website color
          </div>
        </div>
      </div>

      <div className={"bg-" + state.backgroundColor + " pt-12 pb-12"}>
        <div className="w-3/4 xl:w-1/2 mx-auto">
          <div
            className={
              state.primaryTextColor + " text-3xl font-sans leading-snug"
            }
          >
            Change the values in the menu bar, and then view your scheme applied
            to several common web layouts.
          </div>
          <div
            className={
              state.primaryTextColor + " text-xl font-sans leading-snug pt-10"
            }
          >
            By default, the application uses the color values provided by{" "}
            <a
              className={state.secondaryTextColor}
              href="https://tailwindcss.com/"
            >
              Tailwind CSS
            </a>
            . Click on the Settings button in the menu bar to see the Tailwind
            class name and hex value for each of the colors in your scheme.
          </div>
          <div className="py-10 px-20">
            <SettingsHome />
          </div>
          <div
            className={
              state.primaryTextColor + " text-xl font-sans leading-snug"
            }
          >
            As an alternative to the values provide by Tailwind, you can also
            use the{" "}
            <a
              className={state.secondaryTextColor}
              href="https://material.io/design/color/the-color-system.html#"
            >
              Material Design color set
            </a>{" "}
            with this application. Click Settings in the menu bar, and then
            click Use Material Design Colors. To use these colors along with
            Tailwind in your application, add the following to your
            tailwind.config.js file.
          </div>
          <div className="py-10 px-20">
            <Material />
          </div>
        </div>
      </div>
    </AppLayout>
  );
};

export default IndexPage;
