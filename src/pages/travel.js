import React, { useContext } from "react";
import { useStaticQuery, graphql } from "gatsby";
import Img from "gatsby-image";
import { GlobalStateContext } from "../context/GlobalContextProvider";
import AppLayout from "../components/AppLayout";

const Travel = props => {
  const state = useContext(GlobalStateContext);
  const kyotoImg = useStaticQuery(graphql`
    query KyotoQuery {
      file(name: { eq: "kyotobw" }) {
        childImageSharp {
          # Specify the image processing specifications right in the query.
          fluid {
            ...GatsbyImageSharpFluid
          }
        }
      }
    }
  `);
  return (
    <AppLayout pathname={props.path}>
      <div className={"bg-" + state.backgroundColor + " font-sans pt-8 pb-16"}>
        <div
          className={
            "bg-" + state.secondaryColor + " relative w-1/2 xl:w-2/5 mx-auto"
          }
        >
          <div className="absolute bottom-0 z-20 text-white font-inter_bold text-4xl w-full text-center h-full align-middle pt-48">
            <div className=""> Kyoto</div>
          </div>
          <Img
            className="bg-cover opacity-50 mx-auto z-10"
            alt="Bob Humphrey website"
            fluid={kyotoImg.file.childImageSharp.fluid}
          />
        </div>
        <div
          className={
            "bg-" +
            state.secondaryColor +
            " " +
            state.primaryTextColor +
            " w-1/2 xl:w-2/5 text-justify mx-auto p-8"
          }
        >
          With its 2,000 religious places – 1,600 Buddhist temples and 400
          Shinto shrines, as well as palaces, gardens and architecture intact –
          Kyoto is one of the best preserved cities in Japan.
        </div>
        <div
          className={
            state.primaryTextColor +
            " w-1/2 xl:w-2/5 text-sm text-center mx-auto pt-4 pb-4"
          }
        >
          <a
            className=""
            href={"https://unsplash.com/@boontohhgraphy"}
            target="_blank"
            rel="noopener noreferrer"
          >
            Photo credit: Sorasak
          </a>
        </div>
      </div>
    </AppLayout>
  );
};

export default Travel;
