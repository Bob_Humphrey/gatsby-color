import React, { useContext } from "react";
import { GlobalStateContext } from "../context/GlobalContextProvider";
import AppLayout from "../components/AppLayout";
import Puzzle from "../components/svg/Puzzle";

const Form = props => {
  const state = useContext(GlobalStateContext);
  return (
    <AppLayout pathname={props.path}>
      <div
        className={
          "bg-" + state.backgroundColor + " flex justify-center w-full py-24"
        }
      >
        {/* CENTER CONTAINER */}
        <div className="flex lg:w-4/5 xl:w-3/5 font-sans ">
          <div className="w-1/2">
            <Puzzle />
          </div>
          <div className={"bg-white w-1/2 p-10 rounded-lg"}>
            <div
              className={
                state.primaryTextColor +
                " text-2xl font-inter_bold leading-tight"
              }
            >
              We can help you put the pieces back together again
            </div>
            <div className={state.primaryTextColor + " pt-8"}>
              Nulla metus metus, ullamcorper vel, tincidunt sed, euismod in,
              nibh. Quisque volutpat condimentum velit. Class aptent taciti
              sociosqu ad litora torquent per conubia nostra, per inceptos
              himenaeos. Nam nec ante. Sed lacinia, urna non tincidunt mattis,
              tortor neque.
            </div>
            <div className="pt-10">
              <button
                className={
                  "bg-" +
                  state.secondaryColor +
                  " font-inter_bold text-white text-center rounded py-2 w-48"
                }
              >
                Learn More
              </button>
            </div>
          </div>
        </div>
      </div>
    </AppLayout>
  );
};

export default Form;
