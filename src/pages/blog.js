import React, { useContext } from "react";
import { GlobalStateContext } from "../context/GlobalContextProvider";
import AppLayout from "../components/AppLayout";

const Blog = props => {
  const state = useContext(GlobalStateContext);
  return (
    <AppLayout pathname={props.path}>
      <div
        className={
          "bg-" +
          state.backgroundColor +
          " flex justify-center w-full font-fanwood pt-24 pb-16"
        }
      >
        <div className="w-5/6 lg:w-1/2 text-2xl ">
          <div
            className={"border-b border-" + state.primaryColor + " pb-12 mb-12"}
          >
            <h3
              className={
                state.secondaryTextColor +
                " font-bold text-center text-6xl leading-none mb-8"
              }
            >
              Quisque volutpat condimentum
            </h3>
            <p className={state.primaryTextColor + " text-justify mb-12"}>
              Curabitur sodales ligula in libero. Sed dignissim lacinia nunc.
              Curabitur tortor. Pellentesque nibh. Aenean quam. In scelerisque
              sem at dolor. Maecenas mattis. Sed convallis tristique sem. Proin
              ut ligula vel nunc egestas porttitor. Morbi lectus risus, iaculis
              vel, suscipit quis, luctus non, massa. Fusce ac turpis quis ligula
              lacinia aliquet. Mauris ipsum.
              <span className={state.secondaryTextColor + ""}>
                &nbsp; Continue reading
              </span>
            </p>
            <div className={state.primaryTextColor + " text-xl text-center"}>
              Februay 6, 2020
            </div>
          </div>

          <div
            className={"border-b border-" + state.primaryColor + " pb-12 mb-12"}
          >
            <h3
              className={
                state.secondaryTextColor +
                " font-bold text-center text-6xl leading-none mb-8"
              }
            >
              Quisque cursus metus vitae
            </h3>
            <p className={state.primaryTextColor + " text-justify mb-12"}>
              Morbi lacinia molestie dui. Praesent blandit dolor. Sed non quam.
              In vel mi sit amet augue congue elementum. Morbi in ipsum sit amet
              pede facilisis laoreet. Donec lacus nunc, viverra nec, blandit
              vel, egestas et, augue. Vestibulum tincidunt malesuada tellus. Ut
              ultrices ultrices enim. Curabitur sit amet mauris.
              <span className={state.secondaryTextColor + ""}>
                &nbsp; Continue reading
              </span>
            </p>
            <div className={state.primaryTextColor + " text-xl text-center"}>
              Februay 2, 2020
            </div>
          </div>

          <div
            className={"border-b border-" + state.primaryColor + " pb-12 mb-12"}
          >
            <h3
              className={
                state.secondaryTextColor +
                " font-bold text-center text-6xl leading-none mb-8"
              }
            >
              Proin sodales libero
            </h3>
            <p className={state.primaryTextColor + " text-justify mb-12"}>
              Proin sodales libero eget ante. Nulla quam. Aenean laoreet.
              Vestibulum nisi lectus, commodo ac, facilisis ac, ultricies eu,
              pede. Ut orci risus, accumsan porttitor, cursus quis, aliquet
              eget, justo. Sed pretium blandit orci. Ut eu diam at pede suscipit
              sodales. Aenean lectus elit, fermentum non, convallis id, sagittis
              at, neque. Nullam mauris orci, aliquet et, iaculis et, viverra
              vitae, ligula. Nulla ut felis in purus aliquam imperdiet. Maecenas
              aliquet mollis lectus.
              <span className={state.secondaryTextColor + ""}>
                &nbsp; Continue reading
              </span>
            </p>
            <div className={state.primaryTextColor + " text-xl text-center"}>
              January 31, 2020
            </div>
          </div>

          <div
            className={"border-b border-" + state.primaryColor + " pb-12 mb-12"}
          >
            <h3
              className={
                state.secondaryTextColor +
                " font-bold text-center text-6xl leading-none mb-8"
              }
            >
              Vestibulum lacinia arcu eget nulla
            </h3>
            <p className={state.primaryTextColor + " text-justify mb-12"}>
              Class aptent taciti sociosqu ad litora torquent per conubia
              nostra, per inceptos himenaeos. Curabitur sodales ligula in
              libero. Sed dignissim lacinia nunc. Curabitur tortor. Pellentesque
              nibh. Aenean quam. In scelerisque sem at dolor. Maecenas mattis.
              Sed convallis tristique sem. Proin ut ligula vel nunc egestas
              porttitor. Morbi lectus risus, iaculis vel, suscipit quis, luctus
              non, massa.
              <span className={state.secondaryTextColor + ""}>
                &nbsp; Continue reading
              </span>
            </p>
            <div className={state.primaryTextColor + " text-xl text-center"}>
              January 27, 2020
            </div>
          </div>

          <div className="pb-12 mb-12">
            <h3
              className={
                state.secondaryTextColor +
                " font-bold text-center text-6xl leading-none mb-8"
              }
            >
              Sed lacinia
            </h3>
            <p className={state.primaryTextColor + " text-justify mb-12"}>
              Fusce ac turpis quis ligula lacinia aliquet. Mauris ipsum. Nulla
              metus metus, ullamcorper vel, tincidunt sed, euismod in, nibh.
              Quisque volutpat condimentum velit. Class aptent taciti sociosqu
              ad litora torquent per conubia nostra, per inceptos himenaeos. Nam
              nec ante. Sed lacinia, urna non tincidunt mattis, tortor neque
              adipiscing diam, a cursus ipsum ante quis turpis. Nulla facilisi.
              <span className={state.secondaryTextColor + ""}>
                &nbsp; Continue reading
              </span>
            </p>
            <div className={state.primaryTextColor + " text-xl text-center"}>
              Februay 2, 2020
            </div>
          </div>
        </div>
      </div>
    </AppLayout>
  );
};

export default Blog;
