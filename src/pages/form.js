import React, { useContext } from "react";
import { GlobalStateContext } from "../context/GlobalContextProvider";
import AppLayout from "../components/AppLayout";
import Ama from "../components/svg/Ama";

const Form = props => {
  const state = useContext(GlobalStateContext);
  return (
    <AppLayout pathname={props.path}>
      <div
        className={
          "bg-" +
          state.backgroundColor +
          " flex justify-center w-full font-sans"
        }
      >
        {/* CENTER CONTAINER */}
        <div className="flex w-full font-sans ">
          {/* LEFT COLUMN */}
          <div className={"flex bg-" + state.backgroundColor + " w-1/2"}>
            <div
              className={
                "bg-" +
                state.backgroundColor +
                " w-3/5 xl:w-2/5 pt-12 pb-48 mx-auto"
              }
            >
              <div
                className={
                  state.primaryTextColor +
                  " text-3xl font-inter_bold text-center pt-12 pb-10"
                }
              >
                Ask me anything
              </div>
              <Ama />
            </div>
          </div>
          {/* RIGHT COLUMN */}
          <div className={"bg-white w-1/2"}>
            <div className={"container w-3/5 xl:w-1/2 mt-20 mx-auto"}>
              {/* NAME LABELS */}
              <div className="flex text-sm font-inter_bold">
                <label className={state.primaryTextColor + " flex-1 py-2 mx-2"}>
                  Full Name
                </label>
                <label className={state.primaryTextColor + " flex-1 py-2 mx-2"}>
                  User Name
                </label>
              </div>
              {/* NAME INPUTS */}
              <div className="flex">
                <div className="flex-1 mx-2">
                  <input
                    className={
                      "bg-" +
                      state.backgroundColor +
                      " w-full py-2 px-4 rounded"
                    }
                  />
                </div>
                <div className="flex-1 mx-2">
                  <input
                    className={
                      "bg-" +
                      state.backgroundColor +
                      " w-full py-2 px-4 rounded"
                    }
                  />
                </div>
              </div>
              {/* EMAIL LABEL */}
              <div className="mx-2 py-2">
                <label
                  className={
                    state.primaryTextColor + " text-sm font-inter_bold"
                  }
                >
                  Email Address
                </label>
              </div>
              {/* EMAIL INPUT */}
              <div className="mx-2">
                <input
                  className={
                    "bg-" + state.backgroundColor + " w-full py-2 px-4 rounded"
                  }
                />
              </div>
              {/* MESSAGE LABEL */}
              <div className="mx-2 py-2">
                <label
                  className={
                    state.primaryTextColor + " text-sm font-inter_bold"
                  }
                >
                  Message
                </label>
              </div>
              {/* MESSAGE TEXTAREA */}
              <div className="mx-2">
                <textarea
                  row="10"
                  cols="50"
                  className={
                    "bg-" + state.backgroundColor + " w-full py-2 px-4 rounded"
                  }
                />
              </div>
              {/* BUTTON */}
              <div className="">
                <button
                  className={
                    "bg-" +
                    state.primaryColor +
                    " text-center text-white font-inter_bold w-48 + px-4 + py-2 mx-2 mt-4 rounded"
                  }
                >
                  Send
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </AppLayout>
  );
};

export default Form;
