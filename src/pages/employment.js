import React, { useContext } from "react";
import { GlobalStateContext } from "../context/GlobalContextProvider";
import AppLayout from "../components/AppLayout";
import Team from "../components/svg/Team";

const Employment = props => {
  const state = useContext(GlobalStateContext);
  return (
    <AppLayout pathname={props.path}>
      <div
        className={
          "bg-" + state.backgroundColor + " flex justify-center w-full py-32"
        }
      >
        {/* CENTER CONTAINER */}
        <div className="flex lg:w-4/5 xl:w-3/5 font-sans ">
          <div className="w-1/2 pr-16">
            <div
              className={
                state.secondaryTextColor +
                " text-4xl font-inter_bold  leading-tight"
              }
            >
              Find the talent you need for your team
            </div>
            <div className={state.primaryTextColor + " text-lg pt-8"}>
              Sed aliquet risus a tortor. Integer id quam. Morbi mi. Quisque
              nisl felis, venenatis tristique, dignissim in, ultrices sit amet,
              augue.
            </div>
            <div className="pt-12">
              <button
                className={
                  "bg-" +
                  state.primaryColor +
                  " font-inter_bold text-white text-center rounded py-2 w-48"
                }
              >
                Get help now
              </button>
            </div>
          </div>
          <div className="w-1/2">
            <Team />
          </div>
        </div>
      </div>
    </AppLayout>
  );
};

export default Employment;
