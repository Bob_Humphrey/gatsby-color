import React, { useContext } from "react";
import { StaticQuery, graphql } from "gatsby";
import { GlobalStateContext } from "../context/GlobalContextProvider";
import AppLayout from "../components/AppLayout";
import Dog from "../components/Dog";

const DOG_LIST = graphql`
  query DogList {
    allFile(
      filter: {
        relativeDirectory: { eq: "dogs" }
        childImageSharp: { fluid: {} }
      }
    ) {
      edges {
        node {
          id
          childImageSharp {
            fluid {
              ...GatsbyImageSharpFluid
              originalName
            }
          }
        }
      }
    }
  }
`;

const Dogs = props => {
  const state = useContext(GlobalStateContext);
  return (
    <AppLayout pathname={props.path}>
      <div className={"bg-white flex justify-center w-full py-16"}>
        {/* CENTER CONTAINER */}
        <div
          className={
            "text-" + state.primaryColor + " lg:w-4/5 xl:w-3/5 font-sans"
          }
        >
          <div className="flex flex-wrap -mx-3">
            <StaticQuery
              query={DOG_LIST}
              render={({ allFile }) =>
                allFile.edges.map(({ node }) => (
                  <Dog dog={node} key={node.id} />
                ))
              }
            />
          </div>
        </div>
      </div>
    </AppLayout>
  );
};

export default Dogs;
