import React, { useContext } from "react";
import { useStaticQuery, graphql } from "gatsby";
import Img from "gatsby-image";
import { GlobalStateContext } from "../context/GlobalContextProvider";
import AppLayout from "../components/AppLayout";

const Fashion = props => {
  const state = useContext(GlobalStateContext);
  const fashionImg = useStaticQuery(graphql`
    query FashionQuery {
      file(name: { eq: "fashion" }) {
        childImageSharp {
          # Specify the image processing specifications right in the query.
          fluid {
            ...GatsbyImageSharpFluid
          }
        }
      }
    }
  `);
  return (
    <AppLayout pathname={props.path}>
      <div className={"bg-" + state.backgroundColor + " font-sans py-8"}>
        <div className="flex bg-white w-4/5 xl:w-3/5 mx-auto p-8">
          <div className="w-1/2">
            <Img
              className="bg-cover mx-auto"
              alt="Travel blazer"
              fluid={fashionImg.file.childImageSharp.fluid}
            />
          </div>
          <div className={state.primaryTextColor + " w-1/2 pl-12"}>
            <div
              className={
                state.secondaryTextColor + " text-2xl font-inter_bold pb-3"
              }
            >
              Travel Blazer
            </div>
            <div className="text-xl font-inter_bold pb-4">$79.99</div>
            <div className="flex items-center ">
              <div className="text-center border border-gray-400 w-32 py-1 mr-2">
                Regular
              </div>
              <div className="text-center border border-gray-400 w-32 py-1 mr-2">
                Petite
              </div>
              <div className="text-center border border-gray-400 w-32 py-1 mr-2">
                Tall
              </div>
            </div>
            <div className=" flex items-center py-6">
              <div className="mr-3">Colors</div>
              <div className="bg-gray-400 h-6 w-6 mx-3 rounded-lg">&nbsp;</div>
              <div className="bg-gray-800 h-6 w-6 mx-3 rounded-lg">&nbsp;</div>
              <div className="bg-gray-600 h-6 w-6 mx-3 rounded-lg">&nbsp;</div>
              <div className="bg-yellow-800 h-6 w-6 mx-3 rounded-lg">
                &nbsp;
              </div>
            </div>
            <div className="flex items-center pb-2">
              <div className="text-center border border-gray-400 w-16 py-1 mr-2">
                2
              </div>
              <div className="text-center border border-gray-400 w-16 py-1 mr-2">
                4
              </div>
              <div className="text-center border border-gray-400 w-16 py-1 mr-2">
                6
              </div>
              <div className="text-center border border-gray-400 w-16 py-1 mr-2">
                8
              </div>
              <div className="text-center border border-gray-400 w-16 py-1 mr-2">
                10
              </div>
              <div className="text-center border border-gray-400 w-16 py-1 mr-2">
                12
              </div>
            </div>
            <div className="flex items-center pb-4">
              <div className="text-center border border-gray-400 w-16 py-1 mr-2">
                14
              </div>
              <div className="text-center border border-gray-400 w-16 py-1 mr-2">
                16
              </div>
              <div className="text-center border border-gray-400 w-16 py-1 mr-2">
                18
              </div>
              <div className="text-center border border-gray-400 w-16 py-1 mr-2">
                20
              </div>
              <div className="text-center border border-gray-400 w-16 py-1 mr-2">
                22
              </div>
              <div className="text-center border border-gray-400 w-16 py-1 mr-2">
                24
              </div>
            </div>
            <div className="text-sm pb-4">
              <a
                className=""
                href={"https://unsplash.com/@karinatess"}
                target="_blank"
                rel="noopener noreferrer"
              >
                Photo credit: Karina Tess
              </a>
            </div>
            <div className="flex items-center ">
              <div
                className={
                  "bg-" +
                  state.primaryColor +
                  " text-white font-inter_bold text-center w-48 py-2"
                }
              >
                ADD TO CART
              </div>
            </div>
          </div>
        </div>
      </div>
    </AppLayout>
  );
};

export default Fashion;
