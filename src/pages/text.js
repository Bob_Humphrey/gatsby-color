import React, { useContext } from "react";
import { useStaticQuery, graphql } from "gatsby";
import Img from "gatsby-image";
import { GlobalStateContext } from "../context/GlobalContextProvider";
import AppLayout from "../components/AppLayout";

const Text = props => {
  const state = useContext(GlobalStateContext);
  const watchImg = useStaticQuery(graphql`
    query WatchQuery {
      file(name: { eq: "watch" }) {
        childImageSharp {
          # Specify the image processing specifications right in the query.
          fluid {
            ...GatsbyImageSharpFluid
          }
        }
      }
    }
  `);
  return (
    <AppLayout pathname={props.path}>
      <div className={"bg-" + state.backgroundColor + " font-sans py-8"}>
        <div className="flex bg-white w-4/5 xl:w-3/5 mx-auto p-8">
          <div className={state.primaryTextColor + " w-3/5 px-10"}>
            <div
              className={state.secondaryTextColor + " text-4xl font-chunk pb-3"}
            >
              Tempus Fugit
            </div>
            <div className={state.primaryTextColor + " text-lg text-justify"}>
              <p className="pb-8">
                Morbi in ipsum sit amet pede facilisis laoreet. Donec lacus
                nunc, viverra nec, blandit vel, egestas et, augue. Vestibulum
                tincidunt malesuada tellus. Ut ultrices ultrices enim. Curabitur
                sit amet mauris. Morbi in dui quis est pulvinar ullamcorper.
                Nulla facilisi. Integer lacinia sollicitudin massa. Cras metus.
                Sed aliquet risus a tortor. Integer id quam. Morbi mi. Quisque
                nisl felis, venenatis tristique, dignissim in, ultrices sit
                amet, augue.
              </p>
              <p className="pb-4">
                Integer nec odio. Praesent libero. Sed cursus ante dapibus diam.
                Sed nisi. Nulla quis sem at nibh elementum imperdiet. Duis
                sagittis ipsum. Praesent mauris. Fusce nec tellus sed augue
                semper porta. Mauris massa. Vestibulum lacinia arcu eget nulla.
                Class aptent taciti sociosqu ad litora torquent per conubia
                nostra, per inceptos himenaeos. Curabitur sodales ligula in
                libero.
              </p>
            </div>
          </div>
          <div className="w-2/5">
            <Img
              className="bg-cover mx-auto"
              alt="Travel blazer"
              fluid={watchImg.file.childImageSharp.fluid}
            />
          </div>
        </div>
      </div>
    </AppLayout>
  );
};

export default Text;
