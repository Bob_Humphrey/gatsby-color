import React, { useContext } from "react";
import { GlobalStateContext } from "../context/GlobalContextProvider";
import AppLayout from "../components/AppLayout";

const Comparison = props => {
  const state = useContext(GlobalStateContext);
  return (
    <AppLayout pathname={props.path}>
      <div
        className={
          "bg-" +
          state.backgroundColor +
          " flex justify-center w-full font-sans pt-16 pb-16"
        }
      >
        <div className="flex lg:w-3/4 xl:w-1/2 -mx-2">
          {/* COLUMN 1 */}
          <div className="flex flex-col w-1/3 mx-2">
            <div
              className={
                "bg-" +
                state.secondaryColor +
                " w-full text-white text-center text-3xl font-inter_bold py-4 mb-2"
              }
            >
              BASIC
            </div>
            <div
              className={
                state.primaryTextColor +
                " w-full bg-white text-center text-6xl font-inter_bold"
              }
            >
              $25
            </div>
            <div
              className={
                state.secondaryTextColor +
                " w-full bg-white text-center text-xl font-inter_bold pb-4 mb-2"
              }
            >
              $250 / YEAR
            </div>
            <div
              className={
                state.primaryTextColor +
                " w-full bg-white text-center text-xl py-2 mb-2"
              }
            >
              Quisque
            </div>
            <div
              className={
                state.primaryTextColor +
                " w-full bg-white text-center text-xl py-2 mb-2"
              }
            >
              Sed lectus
            </div>
            <div
              className={
                state.primaryTextColor +
                " w-full bg-white text-center text-xl py-2 mb-2"
              }
            >
              Vestibulum
            </div>
            <div
              className={
                state.primaryTextColor +
                " w-full bg-white text-center text-xl py-2 mb-2"
              }
            >
              Proin quam
            </div>
            <div
              className={
                "bg-" +
                state.secondaryColor +
                " flex justify-center w-full py-4 "
              }
            >
              <button className="flex self-center bg-white font-inter_bold cursor-pointer py-1 px-3 rounded focus:outline-none">
                Sign Up
              </button>
            </div>
          </div>

          {/* COLUMN 2 */}
          <div className="flex flex-col w-1/3 mx-2">
            <div
              className={
                "bg-" +
                state.primaryColor +
                " w-full text-white text-center text-3xl font-inter_bold py-10 mb-2 -mt-12"
              }
            >
              STANDARD
            </div>
            <div
              className={
                state.primaryTextColor +
                " w-full bg-white text-center text-6xl font-inter_bold"
              }
            >
              $50
            </div>
            <div
              className={
                state.secondaryTextColor +
                " w-full bg-white text-center text-xl font-inter_bold pb-4 mb-2"
              }
            >
              $500 / YEAR
            </div>
            <div
              className={
                state.primaryTextColor +
                " w-full bg-white text-center text-xl py-2 mb-2"
              }
            >
              Quisque
            </div>
            <div
              className={
                state.primaryTextColor +
                " w-full bg-white text-center text-xl py-2 mb-2"
              }
            >
              Sed lectus
            </div>
            <div
              className={
                state.primaryTextColor +
                " w-full bg-white text-center text-xl py-2 mb-2"
              }
            >
              Vestibulum
            </div>
            <div
              className={
                state.primaryTextColor +
                " w-full bg-white text-center text-xl py-2 mb-2"
              }
            >
              Pellentesque
            </div>
            <div
              className={
                "bg-" + state.primaryColor + " flex justify-center w-full py-8"
              }
            >
              <button className="flex self-center bg-white font-inter_bold cursor-pointer py-1 px-3 rounded focus:outline-none">
                Sign Up
              </button>
            </div>
          </div>

          {/* COLUMN 3 */}
          <div className="flex flex-col w-1/3 mx-2">
            <div
              className={
                "bg-" +
                state.secondaryColor +
                " w-full text-white text-center text-3xl font-inter_bold py-4 mb-2"
              }
            >
              PREMIUM
            </div>
            <div
              className={
                state.primaryTextColor +
                " w-full bg-white text-center text-6xl font-inter_bold"
              }
            >
              $100
            </div>
            <div
              className={
                state.secondaryTextColor +
                " w-full bg-white text-center text-xl font-inter_bold pb-4 mb-2"
              }
            >
              $1000 / YEAR
            </div>
            <div
              className={
                state.primaryTextColor +
                " w-full bg-white text-center text-xl py-2 mb-2"
              }
            >
              Quisque
            </div>
            <div
              className={
                state.primaryTextColor +
                " w-full bg-white text-center text-xl py-2 mb-2"
              }
            >
              Sed lectus
            </div>
            <div
              className={
                state.primaryTextColor +
                " w-full bg-white text-center text-xl py-2 mb-2"
              }
            >
              Suspendisse
            </div>
            <div
              className={
                state.primaryTextColor +
                " w-full bg-white text-center text-xl py-2 mb-2"
              }
            >
              Proin ut ligula
            </div>
            <div
              className={
                "bg-" +
                state.secondaryColor +
                " flex justify-center w-full py-4 "
              }
            >
              <button className="flex self-center bg-white font-inter_bold cursor-pointer py-1 px-3 rounded focus:outline-none">
                Sign Up
              </button>
            </div>
          </div>
        </div>
      </div>
    </AppLayout>
  );
};

export default Comparison;
