import React, { useContext } from "react";
import { GlobalStateContext } from "../context/GlobalContextProvider";
import Settings from "./Settings";

const SettingsHeader = props => {
  const state = useContext(GlobalStateContext);
  const showSettings = state.showSettings ? "flex" : "hidden";

  return (
    <div
      className={
        showSettings +
        " bg-" +
        state.backgroundColor +
        " w-full text-gray-700 font-fanwood text-xl"
      }
    >
      <div className="w-7/12 ml-16">
        <Settings showButtons="yes" />
      </div>
    </div>
  );
};

export default SettingsHeader;
