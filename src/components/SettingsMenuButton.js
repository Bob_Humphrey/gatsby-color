import React, { useContext } from "react";
import {
  GlobalDispatchContext,
  GlobalStateContext
} from "../context/GlobalContextProvider";

const SettingsMenuButton = props => {
  const dispatch = useContext(GlobalDispatchContext);
  const state = useContext(GlobalStateContext);
  const buttonClasses = props.active
    ? "border-gray-700 hover:text-white hover:bg-gray-700 cursor-pointer"
    : "text-gray-300 border-gray-300 cursor-default";
  let type, colorSet;
  switch (props.type) {
    case "cancel":
      type = "SETTINGS_CANCEL";
      colorSet = state.colorSet;
      break;
    case "tailwind":
      type = "SETTINGS_CHANGE";
      colorSet = "tailwind";
      break;
    case "material":
      type = "SETTINGS_CHANGE";
      colorSet = "material";
      break;
    default:
      break;
  }
  return (
    <button
      className={
        buttonClasses +
        " font-sans text-sm rounded-full border focus:outline-none px-2 mx-2"
      }
      onClick={() => {
        dispatch({
          type: type,
          colorSet: colorSet
        });
      }}
    >
      {props.label}
    </button>
  );
};

export default SettingsMenuButton;
