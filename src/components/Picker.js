import React, { useContext } from "react";
import { GlobalStateContext } from "../context/GlobalContextProvider";
import PickerButtonGroup from "./PickerButtonGroup";
import PickerMenuButton from "./PickerMenuButton";

const Picker = () => {
  const state = useContext(GlobalStateContext);
  const showPicker = state.showColorPicker ? "flex" : "hidden";
  const tailwindHues = [
    "gray",
    "red",
    "orange",
    "yellow",
    "green",
    "teal",
    "blue",
    "indigo",
    "purple",
    "pink"
  ];
  const materialHues = [
    "md-red",
    "md-pink",
    "md-purple",
    "md-deep-purple",
    "md-indigo",
    "md-blue",
    "md-light-blue",
    "md-cyan",
    "md-teal",
    "md-green",
    "md-light-green",
    "md-lime",
    "md-yellow",
    "md-amber",
    "md-orange",
    "md-deep-orange",
    "md-brown",
    "md-gray",
    "md-blue-gray"
  ];
  const hues = state.colorSet === "tailwind" ? tailwindHues : materialHues;
  return (
    <div
      className={
        showPicker +
        " bg-" +
        state.backgroundColor +
        " text-gray-700 font-fanwood text-xl"
      }
    >
      <div className="flex bg-white p-10 ml-16">
        <div className="flex flex-col ">
          <div className="flex justify-between w-full mb-3">
            <div className="flex justify-start self-center font-sans font-inter_bold">
              Pick {state.pickerLabel} Color
            </div>
            <div className="flex justify-end py-2">
              <PickerMenuButton
                key="1"
                type="black"
                hue="black"
                property={state.pickerProperty}
              />
              <PickerMenuButton
                key="2"
                type="white"
                hue="white"
                property={state.pickerProperty}
              />
              <PickerMenuButton key="3" type="cancel" hue="" property="" />
            </div>
          </div>
          <div className="flex flex-row">
            {hues.map(function(hue, index) {
              return (
                <div key={index} className="flex flex-col w-12">
                  <PickerButtonGroup
                    hue={hue}
                    property={state.pickerProperty}
                  />
                </div>
              );
            })}
          </div>
        </div>
      </div>
    </div>
  );
};

export default Picker;
