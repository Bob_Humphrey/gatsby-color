import React, { useContext } from "react";
import { GlobalStateContext } from "../context/GlobalContextProvider";
import PickerButton from "./PickerButton";

const PickerButtonGroup = props => {
  const state = useContext(GlobalStateContext);
  const hue = props.hue;
  const tailwindValues = [
    "100",
    "200",
    "300",
    "400",
    "500",
    "600",
    "700",
    "800",
    "900"
  ];
  const materialValues = [
    "50",
    "100",
    "200",
    "300",
    "400",
    "500",
    "600",
    "700",
    "800",
    "900",
    "A100",
    "A200",
    "A400",
    "A700"
  ];
  const materialNeutralValues = [
    "50",
    "100",
    "200",
    "300",
    "400",
    "500",
    "600",
    "700",
    "800",
    "900"
  ];
  let values;
  if (state.colorSet === "tailwind") {
    values = tailwindValues;
  } else if (state.colorSet === "material") {
    if (props.hue === "md-brown") {
      values = materialNeutralValues;
    } else if (props.hue === "md-gray") {
      values = materialNeutralValues;
    } else if (props.hue === "md-blue-gray") {
      values = materialNeutralValues;
    } else {
      values = materialValues;
    }
  }
  return (
    <div className="">
      {values.map(function(value, index) {
        return (
          <PickerButton
            key={index}
            hue={hue}
            value={value}
            property={props.property}
          />
        );
      })}
    </div>
  );
};

export default PickerButtonGroup;
