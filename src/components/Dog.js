import React, { useContext } from "react";
import Img from "gatsby-image";
import { GlobalStateContext } from "../context/GlobalContextProvider";
import getDog from "../functions/getDog";

const Dog = props => {
  const imageData = getDog(props.dog.childImageSharp.fluid.originalName);
  const state = useContext(GlobalStateContext);
  return (
    <div className="flex w-1/3 px-3 mb-6 items-stretch">
      <div className={"bg-" + state.backgroundColor + " rounded-lg"}>
        <div className={"bg-" + state.backgroundColor + " rounded-t-lg"}>
          <Img
            alt={"Dog image by " + imageData.credit}
            className="rounded-t-lg"
            fluid={props.dog.childImageSharp.fluid}
          />
        </div>
        <div className="py-5 px-8">
          <div
            className={
              state.primaryTextColor +
              " flex justify-center text-sm font-normal pt-2 pb-5"
            }
          >
            <a
              className=""
              href={"https://unsplash.com/@" + imageData.url}
              target="_blank"
              rel="noopener noreferrer"
            >
              Photo credit: {imageData.credit}
            </a>
          </div>
          <div
            className={
              state.primaryTextColor +
              " font-normal pb-6 text-justify leading-tight"
            }
          >
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur
            eget elit eu urna.
          </div>
          <div className="flex justify-center">
            <div
              className={
                "bg-" +
                state.primaryColor +
                " text-white rounded px-8 py-1 mr-2 mb-4 mt-2"
              }
            >
              <button className="font-inter_bold">Learn More</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Dog;
