import { Link } from "gatsby";
import React, { useContext } from "react";
import { GlobalStateContext } from "../context/GlobalContextProvider";
import ColorButton from "./ColorButton";
import SettingsButton from "./SettingsButton";

const AppHeader = props => {
  const state = useContext(GlobalStateContext);
  const homeColor =
    props.pathname === "/" ? state.secondaryTextColor : state.primaryTextColor;
  const blogColor =
    props.pathname === "/blog/"
      ? state.secondaryTextColor
      : state.primaryTextColor;
  const comparisonColor =
    props.pathname === "/comparison/"
      ? state.secondaryTextColor
      : state.primaryTextColor;
  const dogsColor =
    props.pathname === "/dogs/"
      ? state.secondaryTextColor
      : state.primaryTextColor;
  const employmentColor =
    props.pathname === "/employment/"
      ? state.secondaryTextColor
      : state.primaryTextColor;
  const formColor =
    props.pathname === "/form/"
      ? state.secondaryTextColor
      : state.primaryTextColor;
  const helpColor =
    props.pathname === "/help/"
      ? state.secondaryTextColor
      : state.primaryTextColor;
  const travelColor =
    props.pathname === "/travel/"
      ? state.secondaryTextColor
      : state.primaryTextColor;
  const fashionColor =
    props.pathname === "/fashion/"
      ? state.secondaryTextColor
      : state.primaryTextColor;
  const textColor =
    props.pathname === "/text/"
      ? state.secondaryTextColor
      : state.primaryTextColor;
  return (
    <header>
      <div className="flex justify-center text-gray-500 font-sans text-4xl text-center pt-4 pb-4">
        <div className="flex justify-between w-11/12">
          <div className="flex self-center">
            <ColorButton label="Background" property="backgroundColor" />
            <ColorButton label="Primary" property="primaryColor" />
            <ColorButton label="Secondary" property="secondaryColor" />
            <ColorButton label="Primary Text" property="primaryTextColor" />
            <ColorButton label="Secondary Text" property="secondaryTextColor" />
            <SettingsButton />
          </div>

          <div className="flex flex-col xl:flex-row justify-end text-base font-sans font-normal self-center">
            <div className="">
              <Link className={homeColor + " mx-2"} to={`/`}>
                Home
              </Link>
              <Link className={employmentColor + " mx-2"} to={`/employment`}>
                Employment
              </Link>
              <Link className={blogColor + " mx-2"} to={`/blog`}>
                Blog
              </Link>
              <Link className={fashionColor + " mx-2"} to={`/fashion`}>
                Fashion
              </Link>
              <Link className={formColor + " mx-2"} to={`/form`}>
                Form
              </Link>
            </div>
            <div className="">
              <Link className={comparisonColor + " mx-2"} to={`/comparison`}>
                Comparison
              </Link>
              <Link className={dogsColor + " mx-2"} to={`/dogs`}>
                Dogs
              </Link>
              <Link className={textColor + " mx-2"} to={`/text`}>
                Text
              </Link>
              <Link className={helpColor + " mx-2"} to={`/help`}>
                Help
              </Link>
              <Link className={travelColor + " mx-2"} to={`/travel`}>
                Travel
              </Link>
            </div>
          </div>
        </div>
      </div>
    </header>
  );
};

export default AppHeader;
