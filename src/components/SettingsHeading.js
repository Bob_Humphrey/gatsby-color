import React from "react";

const SettingsHeading = props => {
  return (
    <div className=" flex text-gray-700 font-sans text-sm border-t border-l border-r border-gray-400 pt-4 pb-4 px-6">
      <div className="w-1/6">
        <div>COLOR</div>
      </div>
      <div className="w-1/3">PROPERTY</div>
      <div className="w-1/3">TAILWIND CSS CLASS</div>
      <div className="w-1/6">HEX VALUE</div>
    </div>
  );
};

export default SettingsHeading;
