import React, { useContext } from "react";
import { GlobalStateContext } from "../context/GlobalContextProvider";
import SettingsMenuButton from "./SettingsMenuButton";
import getColorHexValue from "../functions/getColorHexValue";
import SettingsLine from "./SettingsLine";
import SettingsHeading from "./SettingsHeading";

const Settings = props => {
  const state = useContext(GlobalStateContext);
  const backgroundName = "bg-" + state.backgroundColor;
  const backgroundHexValue = getColorHexValue(state.colorSet, backgroundName);
  const primaryName = "bg-" + state.primaryColor;
  const primaryHexValue = getColorHexValue(state.colorSet, primaryName);
  const secondaryName = "bg-" + state.secondaryColor;
  const secondaryHexValue = getColorHexValue(state.colorSet, secondaryName);
  const primaryTextName = state.primaryTextColor;
  const primaryTextHexValue = getColorHexValue(state.colorSet, primaryTextName);
  const secondaryTextName = state.secondaryTextColor;
  const secondaryTextHexValue = getColorHexValue(
    state.colorSet,
    secondaryTextName
  );
  const title =
    state.colorSet === "tailwind"
      ? "Tailwind CSS Color Set"
      : "Material Design ColorSet";
  const buttonsDisplay = props.showButtons === "yes" ? "flex" : "hidden";
  return (
    <div className="w-full bg-white font-sans p-10 ">
      <div className="flex flex-col border-b border-gray-400">
        <div className={buttonsDisplay + " justify-between w-full mb-3"}>
          <div className="flex justify-start self-center font-inter_bold">
            {title}
          </div>
          <div className="flex justify-end py-2">
            <SettingsMenuButton
              key="1"
              type="tailwind"
              label="Use Tailwind Colors"
              active={state.colorSet === "material"}
            />
            <SettingsMenuButton
              key="2"
              type="material"
              label="Use Material Design Colors"
              active={state.colorSet === "tailwind"}
            />
            <SettingsMenuButton
              key="3"
              type="cancel"
              label="Close"
              active={true}
            />
          </div>
        </div>

        <SettingsHeading />
        <SettingsLine
          key="1"
          color={state.backgroundColor}
          property="Background"
          name={backgroundName}
          hexValue={backgroundHexValue}
        />
        <SettingsLine
          key="2"
          color={state.primaryColor}
          property="Primary"
          name={primaryName}
          hexValue={primaryHexValue}
        />
        <SettingsLine
          key="3"
          color={state.secondaryColor}
          property="Secondary"
          name={secondaryName}
          hexValue={secondaryHexValue}
        />
        <SettingsLine
          key="4"
          color={state.primaryTextColor.replace("text-", "")}
          property="Primary Text"
          name={primaryTextName}
          hexValue={primaryTextHexValue}
        />
        <SettingsLine
          key="5"
          color={state.secondaryTextColor.replace("text-", "")}
          property="Secondary Text"
          name={secondaryTextName}
          hexValue={secondaryTextHexValue}
        />
      </div>
    </div>
  );
};

export default Settings;
