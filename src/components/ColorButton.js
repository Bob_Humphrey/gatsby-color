import React, { useContext } from "react";
import {
  GlobalDispatchContext,
  GlobalStateContext
} from "../context/GlobalContextProvider";

const ColorButton = props => {
  const dispatch = useContext(GlobalDispatchContext);
  const state = useContext(GlobalStateContext);
  const bgColor = state[props.property].replace("text-", "");
  const border = bgColor === "white" ? "border border-gray-400" : "";
  return (
    <button
      className="flex w-24 justify-center cursor-pointer focus:outline-none"
      onClick={() => {
        dispatch({
          type: `PICK_COLOR`,
          property: props.property,
          label: props.label
        });
      }}
    >
      <div className="flex flex-col justify-center self-center">
        <div
          className={
            border + " bg-" + bgColor + " self-center h-8 w-8 mx-3 rounded-full"
          }
        >
          &nbsp;
        </div>
        <div className="text-xs self-center font-sans font-normal pt-2">
          {props.label}
        </div>
      </div>
    </button>
  );
};

export default ColorButton;
