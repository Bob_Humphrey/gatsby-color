import React, { useContext } from "react";
import { GlobalDispatchContext } from "../context/GlobalContextProvider";

const SettingsButton = props => {
  const dispatch = useContext(GlobalDispatchContext);
  const border = "border border-gray-400";
  return (
    <button
      className="flex w-24 justify-center cursor-pointer focus:outline-none"
      onClick={() => {
        dispatch({
          type: `SETTINGS`,
          property: props.property,
          label: props.label
        });
      }}
    >
      <div className="flex flex-col justify-center self-center">
        <div
          className={border + " bg-white self-center h-8 w-8 mx-3 rounded-full"}
        >
          &nbsp;
        </div>
        <div className="text-xs self-center font-sans font-normal pt-2">
          Settings
        </div>
      </div>
    </button>
  );
};

export default SettingsButton;
