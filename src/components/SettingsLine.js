import React from "react";

const SettingsLine = props => {
  const border = props.color === "white" ? "border border-gray-400" : "";

  return (
    <div className=" flex text-gray-700 font-sans border-t border-l border-r border-gray-400 pt-4 pb-4 px-6">
      <div className="w-1/6">
        <div
          className={
            border + " bg-" + props.color + " self-center h-8 w-8 rounded-full"
          }
        >
          &nbsp;
        </div>
      </div>
      <div className="w-1/3">{props.property}</div>
      <div className="w-1/3">{props.name}</div>
      <div className="w-1/6">{props.hexValue}</div>
    </div>
  );
};

export default SettingsLine;
