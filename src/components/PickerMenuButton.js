import React, { useContext } from "react";
import { GlobalDispatchContext } from "../context/GlobalContextProvider";

const PickerMenuButton = props => {
  const dispatch = useContext(GlobalDispatchContext);
  const type = props.type === "cancel" ? "PICK_COLOR_CANCEL" : "CHANGE_COLOR";
  return (
    <button
      className="font-sans text-sm rounded-full border border-gray-700 hover:text-white hover:bg-gray-700 focus:outline-none px-2 mx-2"
      onClick={() => {
        dispatch({
          type: type,
          property: props.property,
          hue: props.hue,
          value: ""
        });
      }}
    >
      {props.type.charAt(0).toUpperCase() + props.type.substring(1)}
    </button>
  );
};

export default PickerMenuButton;
