import SEO from "../components/seo";
import React from "react";
import AppHeader from "./AppHeader";
import Picker from "./Picker";
import SettingsHeader from "./SettingsHeader";
import Footer from "./Footer";

const AppLayout = ({ children, pathname }) => {
  return (
    <div>
      <div className="block lg:hidden w-11/12 mt-32 text-2xl text-center mx-auto">
        <div className="text-gray-800 text-3xl font-chunk text-center leading-tight">
          A simple tool
        </div>
        <div className="text-gray-500 text-3xl font-chunk text-center leading-tight">
          for working with website color
        </div>
        <div className="pt-10">
          A larger display is required for this application.
        </div>
      </div>
      <div className="hidden lg:block">
        <SEO keywords={[`color`, `tailwind css`]} title="Color Schemer" />
        <AppHeader pathname={pathname} />
        <Picker />
        <SettingsHeader />
        <main className="">{children}</main>
        <Footer />
      </div>
    </div>
  );
};

export default AppLayout;
