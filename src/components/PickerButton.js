import React, { useContext } from "react";
import { GlobalDispatchContext } from "../context/GlobalContextProvider";

const PickerButton = props => {
  const dispatch = useContext(GlobalDispatchContext);
  const bgColor = "bg-" + props.hue + "-" + props.value;
  return (
    <button
      className="flex flex-col justify-center cursor-pointer p-1 focus:outline-none"
      onClick={() => {
        dispatch({
          type: `CHANGE_COLOR`,
          property: props.property,
          hue: props.hue,
          value: props.value
        });
      }}
    >
      <div className={bgColor + " self-center h-8 w-8 rounded-full"}>
        &nbsp;
      </div>
    </button>
  );
};

export default PickerButton;
