import React, { useContext } from "react";
import { GlobalStateContext } from "../context/GlobalContextProvider";
import Settings from "./Settings";

const SettingsHome = props => {
  const state = useContext(GlobalStateContext);
  const showSettings = "flex";

  return (
    <div
      className={
        showSettings +
        " bg-" +
        state.backgroundColor +
        " w-full text-gray-700 font-fanwood text-xl"
      }
    >
      <Settings showButtons="no" />
    </div>
  );
};

export default SettingsHome;
